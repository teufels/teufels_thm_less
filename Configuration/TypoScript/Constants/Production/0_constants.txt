plugin.tx_teufels_thm_less {
    settings {
        production {
            includePath {
                public = EXT:teufels_thm_less/Resources/Public/
                private = EXT:teufels_thm_less/Resources/Private/
                frontend {
                    public = typo3conf/ext/teufels_thm_less/Resources/Public/
                }
            }
        }
    }
}